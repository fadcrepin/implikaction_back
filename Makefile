.DEFAULT_GOAL := help	
# Show this help message
# Multiple lines are supported too
# When it's first, both commands works:
#  - `make`
#  - `make help`
help: 
	@cat $(MAKEFILE_LIST) | docker run --rm -i xanders/make-help | more

################################# ENV COLOR #################################
C1 = \e[92m↑
C0 = SUCCES ↑\e[0m\e[39m

################################# ENV VARIABLE #################################
APP_NAME = backend

################################# ENV ARG #################################
args1 = $(filter-out $@,$(wordlist 2,2,$(MAKECMDGOALS)))
args2 = $(filter-out $@,$(wordlist 3,3,$(MAKECMDGOALS)))

%:
	@echo 'Commands make not found, for list commands executed « make help »'

################################################################################
##
## ____ BASI ____
##
################################################################################
# Start your app, up, load and browser
start: 
		docker-compose up

# Builds, (re)creates, starts, and attaches to containers for a service.
#  - `ARGUMENT1` = Name target container, Default target all container
up:
	docker-compose up -d ${args1}
	@echo "${C1} UP ${C0}"

# Stops containers and removes containers, networks, volumes, and images created by up.
down:
	docker-compose down
	@echo "${C1} DOWN ${C0}"



# Stream in you terminal the logs in format json for the target container. 
#  - `ARGUMENT1` = Name target container, Default  target container is this app
logs:
	docker-compose logs -f $(if ${args1},${args1},${APP_NAME})

# Restart.
restart: down start

# Displays the status of containers. 
#  - `ARGUMENT1` = Name target container, Default target all container
status: 
	docker-compose ps ${args1}

################################################################################
## 
## ____ LOAD ____
## 
################################################################################
# creata db table or add info in db here



# ################################################################################
# ## 
# ## ____ SED ____
# ## 
# ################################################################################
# sed addr


################################################################################
## 
## ____ OTHER ____
## 
################################################################################
# Start terminal bash ine a container
#  - `ARGUMENT1` = Name container. Default, this app container name
bash:
	docker-compose exec $(if ${args1},${args1},${APP_NAME}) bash