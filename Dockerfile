FROM node:15.4

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm i -g @nestjs/cli
RUN npm install
COPY . ./

CMD npm run start:dev